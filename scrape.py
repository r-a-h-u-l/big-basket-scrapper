# all imports
from bs4 import BeautifulSoup
import requests
import csv

import time

from selenium import webdriver
# selenium setup
options = webdriver.ChromeOptions()
options.add_argument("--ignore-certificate-errors")
options.add_argument("--test-type")
options.binary_location = "/usr/bin/chromium"
driver = webdriver.Chrome("/Users/iblinfotech/Desktop/chromedriver")
page_url = "https://www.bigbasket.com/pc/beverages/tea/?nc=nb"
driver.get(page_url)

# open file to store data
# scrolling page to load product it scroll 6 times
SCROLL_PAUSE_TIME = 2

# Get scroll height
last_height = driver.execute_script("return document.body.scrollHeight")

while True:
    # Scroll down to bottom
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load page
    time.sleep(SCROLL_PAUSE_TIME)

    # Calculate new scroll height and compare with last scroll height
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        element = driver.find_element_by_tag_name("body")
        driver.execute_script("return arguments[0].scrollIntoView(true);", element)
        break
    last_height = new_height
time.sleep(2)

# clicking on show more button so we will get more product
while True:
    try:
        show_more = driver.find_element_by_class_name("show-more")
        show_more.click()
        time.sleep(2)
    except:
        break

scrape = open("product_data.csv", "w")
scrape_Writer = csv.writer(scrape)
scrape_Writer.writerow(
    [
        "Brand",
        "Product Name",
        "Price",
        "Rating",
        "Product Url",
        "Image Url",
        "Other Info",
    ]
)
for product_div in driver.find_elements_by_class_name("col-xs-5"):
    # get all links of products and scrape data
    for a in product_div.find_elements_by_xpath(".//a"):
        url = a.get_attribute("href")
        print(url)
        source = requests.get(url).text
        soup = BeautifulSoup(source, "lxml")
        product = soup.find("div", class_="uYU-m")
        # get basic product info
        # brand
        try:
            brand_name = product.a.text
        except:
            brand_name = ""

        # product name
        try:

            product_name = product.h1.text
        except:
            product_name = ""

        # product price
        price = product.find("td", class_="IyLvo").text

        # product rating
        try:
            rating = product.find("div", class_="_1AXTE").text
            rating = rating.split()[0]
        except:
            rating = "No Rating"
        # product image url
        try:
            product_image_url = soup.find("div", class_="_2FbOx").img["src"]
        except:
            product_image_url = "No url for image"

        # other product info such as [about product,how to use, ingrediants,nutritional facts]
        other_info = soup.find("section", class_="_3JQUe")
        other_dict = {}
        try:
            for div in other_info("div", class_="_3ezVU"):
                data = div.text
                data = data.split("\n")
                other_dict[data[0]] = data[2]
        except:
            continue

        # write data to csv file
        scrape_Writer.writerow(
            [
                brand_name,
                product_name,
                price,
                rating,
                url,
                product_image_url,
                other_dict,
            ]
        )
        time.sleep(1)
scrape.close()
# close selenium chrome window
driver.close()