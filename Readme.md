# <h1 align="center">Big basket Scrapper</h1>

## About The Project
 
The aim of this project is to scrape data of product from big basket website

### Built With
 
* [Python](https://www.python.org/)

### Installation
1. install requirements.txt file using `pip install -r requirements.txt`
2. run scrape.py file and you will get output in 'scrape_data.csv' file

## Note
Just Because we're scrapping data from live website if we request continuously then it will block our request for some time so to avoid this problem I put the code in sleep mode for some seconds and it will result in increase of execution time
